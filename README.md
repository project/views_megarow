# Views Megarow

Views megarow is a module letting you display menu callback between two results
of a view.
The module can be used to preview content, build quick edit forms,
display more complex forms without having to go to a dedicated page.

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers

## Requirements

Depends on views module.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Navigate to Administration > Extend and enable the module. 
2. Administration > Configuration > User interface > Views Megarow configuration. 
3. Set configuration for views megarows.

**In order to use it**

1. Create a new view 
2. Select the "Megarow table" format 
3. Add a "Megarow links" field 
4. In this field enter one megarow link per line, the structure of a link is the 
link title and its path joined with a pipe sign (|) (eg: Preview|node/1). 
5. Save your view and display your table 
6. When you will click on a link, Drupal will load what's behind this page and 
will render it as the megarow content below the current result of the view.

## Maintainers

- Julien Dubois - [Artusamak](https://www.drupal.org/u/artusamak)
- Bojan Živanović - [bojanz](https://www.drupal.org/u/bojanz)
- Guillaume Bec - [Bès](https://www.drupal.org/u/bès)
- Bohdan Vakulin - [bvakulin](https://www.drupal.org/u/bvakulin)
