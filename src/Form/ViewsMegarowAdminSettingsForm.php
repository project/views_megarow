<?php

namespace Drupal\views_megarow\Form;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Menu\MenuLinkManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form for Admin Views Megarow.
 *
 * @package Drupal\admin_views_megarow\Form
 */
class ViewsMegarowAdminSettingsForm extends ConfigFormBase {

  /**
   * The cache menu instance.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheMenu;

  /**
   * The menu link manager instance.
   *
   * @var \Drupal\Core\Menu\MenuLinkManagerInterface
   */
  protected $menuLinkManager;

  /**
   * ViewsMegarowAdminSettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Menu\MenuLinkManagerInterface $menuLinkManager
   *   A menu link manager instance.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheMenu
   *   A cache menu instance.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    MenuLinkManagerInterface $menuLinkManager,
    CacheBackendInterface $cacheMenu
  ) {
    parent::__construct($configFactory);
    $this->menuLinkManager = $menuLinkManager;
    $this->cacheMenu = $cacheMenu;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.menu.link'),
      $container->get('cache.menu')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'admin_views_megarow.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'admin_views_megarow_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('admin_views_megarow.settings');
    $form['views_megarow_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Megarow title'),
      '#description' => $this->t('Title of the megarow displayed on top of the content.'),
      '#size' => 30,
      '#default_value' => !empty($config->get('views_megarow_title')) ? $config->get('views_megarow_title') : $this->t('Megarow content'),
    ];
    $form['views_megarow_override'] = [
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#title' => $this->t('Override settings'),
    ];
    $form['views_megarow_override']['description'] = [
      '#markup' => $this->t('<p>Disabling these overrides will allow other modules to override the menu paths. <br />
        This is useful if you want some paths to be overriden by panels for instance.</p>'),
    ];
    $form['views_megarow_override']['views_megarow_override_node_edit'] = [
      '#type' => 'radios',
      '#title' => $this->t('Override the node edit form with Views Megarow'),
      '#default_value' => !empty($config->get('views_megarow_override_node_edit'))
        ? $config->get('views_megarow_override_node_edit')
        : 1,
      '#options' => [
        $this->t('Disable'),
        $this->t('Enable'),
      ],
      '#description' => $this->t('Allows Views Megarow to override the path node/{node}/edit.'),
    ];
    $form['views_megarow_override']['views_megarow_override_user_edit'] = [
      '#type' => 'radios',
      '#title' => $this->t('Override the user edit form with Views Megarow'),
      '#default_value' => !empty($config->get('views_megarow_override_user_edit'))
        ? $config->get('views_megarow_override_user_edit')
        : 1,
      '#options' => [
        $this->t('Disable'),
        $this->t('Enable'),
      ],
      '#description' => $this->t('Allows Views Megarow to override the path user/{user}/edit.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('admin_views_megarow.settings')
      ->set('views_megarow_title', $form_state->getValue('views_megarow_title'))
      ->set('views_megarow_override_node_edit', $form_state->getValue('views_megarow_override_node_edit'))
      ->set('views_megarow_override_user_edit', $form_state->getValue('views_megarow_override_user_edit'))
      ->save();
    parent::submitForm($form, $form_state);
    $this->cacheMenu->invalidateAll();
    $this->menuLinkManager->rebuild();
  }

}
