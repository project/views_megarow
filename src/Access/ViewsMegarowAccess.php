<?php

namespace Drupal\views_megarow\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\views\Views;

/**
 * Class access to views_megarow route.
 *
 * @package Drupal\views_megarow\Access
 */
class ViewsMegarowAccess {

  /**
   * Access for the views_megarow path.
   *
   * @param string $view_name
   *   The view name.
   * @param string $display
   *   The view display.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function access(string $view_name, string $display): AccessResult {
    $access_result = AccessResult::forbidden();

    $view = Views::getView($view_name);
    if ($view) {
      $view->setDisplay($display);
      if ($view->access($display)) {
        $access_result = AccessResult::allowed();
      }
    }

    return $access_result;
  }

}
