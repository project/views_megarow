<?php

namespace Drupal\views_megarow\Ajax;

use Drupal\Core\Ajax\CommandInterface;
use Drupal\Core\Ajax\CommandWithAttachedAssetsTrait;

/**
 * An AJAX command for place HTML within the megarow.
 *
 * @ingroup ajax
 */
class MegarowDisplay implements CommandInterface {

  use CommandWithAttachedAssetsTrait;

  /**
   * The entity id.
   *
   * @var int
   */
  protected int $entityId;

  /**
   * The title of the megarow.
   *
   * @var string
   */
  protected string $title;

  /**
   * The html to place within the megarow.
   *
   * @var array|string
   */
  protected array|string $content;

  /**
   * Constructs a MegarowDisplay object.
   *
   * @param int $entityId
   *   The entity id.
   * @param string $title
   *   The title of the megarow.
   * @param array|string $content
   *   The html to place within the megarow.
   */
  public function __construct(int $entityId, string $title, array|string $content) {
    $this->entityId = $entityId;
    $this->title = $title;
    $this->content = $content;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'megarow_display',
      'entity_id' => $this->entityId,
      'title' => $this->title,
      'output' => $this->getRenderedContent(),
    ];
  }

}
