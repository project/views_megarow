<?php

namespace Drupal\views_megarow\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * An AJAX command for dismiss the megarow.
 *
 * @ingroup ajax
 */
class MegarowDismiss implements CommandInterface {

  /**
   * The entity id.
   *
   * @var int
   */
  protected int $entityId;

  /**
   * Constructs a MegarowDisplay object.
   *
   * @param int $entityId
   *   The entity id.
   */
  public function __construct(int $entityId) {
    $this->entityId = $entityId;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'megarow_dismiss',
      'entity_id' => $this->entityId,
    ];
  }

}
