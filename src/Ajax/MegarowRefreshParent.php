<?php

namespace Drupal\views_megarow\Ajax;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Ajax\CommandInterface;

/**
 * An AJAX command for refresh the parent row of a megarow.
 *
 * @ingroup ajax
 */
class MegarowRefreshParent implements CommandInterface {

  /**
   * The unique ID of the base table entity.
   *
   * @var int
   */
  protected int $entityId;

  /**
   * The display_id of the view where the refresh target is displayed.
   *
   * @var string
   */
  protected string $displayId;

  /**
   * An array of arguments the view needs to function.
   *
   * @var array
   */
  protected array $args;

  /**
   * Constructs a MegarowRefreshParent object.
   *
   * @param int $entityId
   *   The unique ID of the base table entity.
   * @param string $displayId
   *   The display_id of the view where the refresh target is displayed.
   * @param array $args
   *   An array of arguments the view needs to function.
   */
  public function __construct(int $entityId, string $displayId, array $args) {
    $this->entityId = $entityId;
    $this->displayId = $displayId;
    $this->args = $args;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'megarow_refresh_parent',
      'entity_id' => $this->entityId,
      'display_id' => $this->displayId,
      'args' => empty($this->args) ? [$this->entityId] : Json::encode($this->args),
    ];
  }

}
