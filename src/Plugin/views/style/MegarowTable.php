<?php

namespace Drupal\views_megarow\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\views\Plugin\views\style\Table;

/**
 * Style plugin to open views links in a modal.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "views_megarow_table",
 *   title = @Translation("Megarow table"),
 *   help = @Translation("Open views links in a modal."),
 *   theme = "views_view_table",
 *   display_types = {"normal"}
 * )
 */
class MegarowTable extends Table {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['autoclose'] = [
      'default' => FALSE,
      'bool' => TRUE,
    ];
    $options['enable_scroll'] = [
      'default' => TRUE,
      'bool' => TRUE,
    ];
    $options['scroll_padding'] = [
      'default' => 120,
    ];
    $options['loading_text'] = [
      'default' => 'Loading...',
      'translatable' => TRUE,
    ];
    $options['close'] = [
      'default' => 'x',
      'translatable' => TRUE,
    ];

    return $options;
  }

  /**
   * Render the given style.
   *
   * The options form will use
   * template_preprocess_views_secondary_row_style_plugin_table.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['megarow'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Megarow settings'),
      '#collapsible' => TRUE,
      '#tree' => FALSE,
    ];
    $form['megarow']['autoclose'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Automatically close the megarow'),
      '#default_value' => $this->options['autoclose'],
      '#description' => $this->t('Automatically close the megarow after submitting a form. (This is only working with nodes and megarow form wrappers.)'),
    ];
    $form['megarow']['enable_scroll'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable scroll'),
      '#default_value' => $this->options['enable_scroll'],
      '#description' => $this->t('Enable the scroll of the page to the row that has just been closed.'),
    ];
    $form['megarow']['scroll_padding'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Scroll padding'),
      '#default_value' => $this->options['scroll_padding'],
      '#field_suffix' => 'px',
      '#size' => 3,
      '#description' => $this->t('Padding between the closed row and the top of the page, 120px is for instance the height of the admin + shortcurts bar.'),
      '#states' => [
        'visible' => [
          ':input[name="style_options[megarow][enable_scroll]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['megarow']['loading_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Loading text'),
      '#default_value' => $this->options['loading_text'],
      '#description' => $this->t('Text displayed while the megarow content is loaded.'),
    ];
    $form['megarow']['close'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Close text'),
      '#default_value' => $this->options['close'],
      '#required' => TRUE,
      '#size' => 8,
      '#description' => $this->t("Text displayed to behave as the megarow's close button."),
    ];

  }

  /**
   * {@inheritdoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state) {
    parent::submitOptionsForm($form, $form_state);
    $form_state->setValue(['style_options', 'autoclose'], $form_state->getValue('autoclose'));
    $form_state->setValue(['style_options', 'enable_scroll'], $form_state->getValue('enable_scroll'));
    $form_state->setValue(['style_options', 'scroll_padding'], $form_state->getValue('scroll_padding'));
    $form_state->setValue(['style_options', 'loading_text'], $form_state->getValue('loading_text'));
    $form_state->setValue(['style_options', 'close'], $form_state->getValue('close'));
  }

}
