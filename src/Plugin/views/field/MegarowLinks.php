<?php

namespace Drupal\views_megarow\Plugin\views\field;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\Token;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Megarow handler that outputs the links which open the megarow.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("megarow_links")
 */
class MegarowLinks extends FieldPluginBase {

  /**
   * Constructs a MegarowLinks object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The id of the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Utility\Token $token
   *    The token service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected Token $token
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('token')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['megarow']['contains']['links']['default'] = '';

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $form['megarow'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Megarow Settings'),
    ];
    $form['megarow']['links'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Links'),
      '#description' => $this->t('Enter one link per line, in the format label|url.'),
      '#default_value' => $this->options['megarow']['links'],
    ];

    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * Returns the available token type based on the view default entity type.
   *
   * @return string
   *   Token type.
   *
   * @throws \Exception
   */
  protected function getTokenType() {
    static $token_type;

    if (empty($token_type)) {
      $token_type = str_replace('_', '-', $this->getEntityType());
    }

    // Hardcode the pattern for taxonomy terms.
    if ($token_type === 'taxonomy-term') {
      $token_type = 'term';
    }
    return $token_type;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    // Get the entity matching this row.
    $entity = $values->_entity;
    $entity_id = $entity->id();

    // Create an array of links.
    $provided_links = explode("\n", $this->options['megarow']['links']);
    $provided_links = array_map('trim', $provided_links);
    $provided_links = array_filter($provided_links, 'strlen');

    $links = [];
    $tokens = $this->getRenderTokens([]);
    foreach ($provided_links as $link) {
      $link_parts = explode('|', $link);

      // Replace tokens if necessary in the url.
      $url = $this->replaceTokens($link_parts[1], $entity);
      $url = $this->renderAltered(['text' => $url], $tokens);

      // Do the same for the label.
      $label = $this->replaceTokens($link_parts[0], $entity);
      $label = $this->renderAltered(['text' => $label], $tokens);
      $label = Html::decodeEntities($label);

      // Add the link for rendering.
      $links[] = $this->getLink($label, $url, [
        'data-dialog-type' => 'megarows',
        'class' => [
          'views-megarow-open',
          'use-ajax',
        ],
      ]);
    }

    $nb_links = count($links);
    if ($nb_links === 0) {
      $element = [];
    }
    else {
      $url = Url::fromUri(
        'internal:/' . $links[0]['href'],
        ['query' => ['display_megarow' => $entity_id]]
      );

      $element = [
        '#type' => 'link',
        '#title' => $links[0]['title'],
        '#url' => $url,
        '#attributes' => $links[0]['attributes'],
      ];
    }

    return $element;
  }

  /**
   * Creates the url replacing the tokens.
   *
   * @param string $raw_url
   *   The raw url.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity object.
   *
   * @return string
   *   The token result.
   *
   * @throws \Exception
   */
  protected function replaceTokens(string $raw_url, EntityInterface $entity): string {
    $data = [
      $this->getTokenType() => $entity,
    ];

    return $this->token->replace($raw_url, $data);
  }

  /**
   * Returns a link as a renderable array.
   *
   * @param string $title
   *   The title.
   * @param string $url
   *   The URL string.
   * @param array $attributes
   *   The attribute array.
   *
   * @return array
   *   The link attributes.
   */
  protected function getLink(string $title, string $url, array $attributes = []): array {
    return ['title' => $title, 'href' => $url, 'attributes' => $attributes];
  }

  /**
   * Returns a prefix if we need for the operation element.
   *
   * Returns empty string, but this will allow us to rewrite the output more
   * easily when we extend the handler.
   *
   * @param \Drupal\search_api\Plugin\views\ResultRow $values
   *   The Views result row.
   *
   * @return string
   *   Element prefix.
   */
  protected function getElementPrefix(ResultRow $values): string {
    return '';
  }

  /**
   * Returns a suffix if we need for the operation element.
   *
   * Returns empty string, but this will allow us to rewrite the output more
   * easily when we extend the handler.
   *
   * @param \Drupal\search_api\Plugin\views\ResultRow $values
   *   The Views result row.
   *
   * @return string
   *   Element suffix.
   */
  protected function getElementSuffix(ResultRow $values): string {
    return '';
  }

}
