<?php

namespace Drupal\views_megarow\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\views\Views;

/**
 * Provides a controller for the views_megarow link type.
 */
class ViewsMegarowController extends ControllerBase {

  use StringTranslationTrait;

  /**
   * Page callback megarow_refresh_parent ajax command.
   *
   * @param string $view_name
   *   The machine name of the view to load.
   * @param string $display
   *   The display_id for the view.
   * @param string|null $args
   *   Arguments to be passed to the view, formatted in JSON.
   *
   * @return array|string|null
   *   The rendered view.
   */
  public function refresh(string $view_name, string $display, string $args = NULL) {
    $output = '';
    $view = Views::getView($view_name);
    if ($view) {
      $view->setDisplay($display);
      // Contextual relationship filter.
      if (!is_null($args)) {
        $view->setArguments(Json::decode($args));
      }
      $output = $view->preview();
    }
    return $output;
  }

}
